INSERT INTO animal (birtdate,name,weight) VALUES ('01-01-2021','Cerdo-001',1.87);
INSERT INTO animal (birtdate,name,weight) VALUES ('01-01-2021','Cerdo-002',1.6);
INSERT INTO animal (birtdate,name,weight) VALUES ('01-01-2021','Cerdo-003',1.65);
INSERT INTO animal (birtdate,name,weight) VALUES ('04-02-2021','Cerdo-004',1.56);
INSERT INTO animal (birtdate,name,weight) VALUES ('04-02-2021','Cerdo-005',1.6);
INSERT INTO animal (birtdate,name,weight) VALUES ('04-02-2021','Cerdo-006',1.89);
INSERT INTO animal (birtdate,name,weight) VALUES ('04-02-2021','Cerdo-007',1.4);



INSERT INTO litter (description,name) VALUES ('Camada nacida el 01-01-2021','Cam-001');
INSERT INTO litter (description,name) VALUES ('Camada nacida el 04-02-2021','Cam-002');
INSERT INTO litter (description,name) VALUES ('Camada nacida el 06-03-2021','Cam-003');
INSERT INTO litter (description,name) VALUES ('Camada nacida el 01-04-2021','Cam-004');


INSERT INTO animal_litter (id_litter,id_animal) VALUES (1,1);
INSERT INTO animal_litter (id_litter,id_animal) VALUES (1,2);
INSERT INTO animal_litter (id_litter,id_animal) VALUES (1,3);
INSERT INTO animal_litter (id_litter,id_animal) VALUES (2,4);
INSERT INTO animal_litter (id_litter,id_animal) VALUES (2,5);
INSERT INTO animal_litter (id_litter,id_animal) VALUES (2,6);
INSERT INTO animal_litter (id_litter,id_animal) VALUES (2,7);


INSERT INTO equipment (amount,name,description) VALUES (5,'Bebedero','Bebederos automaticos');
INSERT INTO equipment (amount,name,description) VALUES (1,'Bascula','Bascula de pataforma para pesar los cerdos');
INSERT INTO equipment (amount,name,description) VALUES (2,'Balanza ','Balanza para le peso de alimento');
INSERT INTO equipment (amount,name,description) VALUES (10,'Geringas','Gerigas para la inyeccion de medicamentos');
INSERT INTO equipment (amount,name,description) VALUES (15,'Alimento','Bulto de alimento para cerdos de levante');
INSERT INTO equipment (amount,name,description) VALUES (15,'Alimento','Bulto de alimento para cerdos de engorde');
INSERT INTO equipment (amount,name,description) VALUES (15,'Alimento','Bulto de alimento para cerdos de ceba o finalizador');
INSERT INTO equipment (amount,name,description) VALUES (15,'Alimento','Bulto de alimento para cerdos de iniciacion');
INSERT INTO equipment (amount,name,description) VALUES (3,'Mangueras','Mangueras para el lavado de las cocheras y el baño de los cerdos');
INSERT INTO equipment (amount,name,description) VALUES (5,'Lazos','Lazos o manilas para la manipulacion de los cerdos');
INSERT INTO equipment (amount,name,description) VALUES (3,'Palas','Palas para oficios varios');



INSERT INTO user (id,created_at,created_by,status,updated_at,updated_by,email,full_name,identification,password,phone,user_rol) VALUES (1,now(),1,'ACTIVE',now(),1,'admin@yopmail.com','admin admin','123','$2a$10$RotZgJiCCP2X8bM.K6vSJeZkr/mugU8S9o9tPfrA1NtgSV3QHdXXi','123','ADMIN');
INSERT INTO user (id,created_at,created_by,status,updated_at,updated_by,email,full_name,identification,password,phone,user_rol) VALUES (2,now(),1,'ACTIVE',now(),1,'customer@yopmail.com','customer customer','456','$2a$10$RotZgJiCCP2X8bM.K6vSJeZkr/mugU8S9o9tPfrA1NtgSV3QHdXXi','456','CUSTOMER');
INSERT INTO user (id,created_at,created_by,status,updated_at,updated_by,email,full_name,identification,password,phone,user_rol) VALUES (3,now(),1,'ACTIVE',now(),1,'seller@yopmail.com','seller seller','789','$2a$10$RotZgJiCCP2X8bM.K6vSJeZkr/mugU8S9o9tPfrA1NtgSV3QHdXXi','789','SELLER');
