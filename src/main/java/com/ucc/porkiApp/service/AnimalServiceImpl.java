package com.ucc.porkiApp.service;

import com.ucc.porkiApp.domain.dto.AnimalDTO;
import com.ucc.porkiApp.domain.dto.AnimalForm;
import com.ucc.porkiApp.domain.dto.LitterDTO;
import com.ucc.porkiApp.domain.dto.MasiveAnimalUpdateForm;
import com.ucc.porkiApp.domain.entity.Animal;
import com.ucc.porkiApp.domain.entity.AnimalLitter;
import com.ucc.porkiApp.domain.entity.Litter;
import com.ucc.porkiApp.domain.repository.AnimalLitterRepository;
import com.ucc.porkiApp.domain.repository.AnimalRepository;
import com.ucc.porkiApp.domain.repository.LitterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional

public class AnimalServiceImpl implements AnimalService {
    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private LitterRepository litterRepository;

    @Autowired
    private AnimalLitterRepository animalLitterRepository;

    @Override
    public List<Animal> getAllAnimalsOrderByName() {
        return animalRepository.findAllByOrderByName() ;
    }


    @Override
    public Animal getByName(String name) {
        return this.animalRepository.getByName(name).orElse(new Animal());
    }



    @Override
    public List<Animal> getAllAnimalsByLitterName(String litterName) {
        return this.animalRepository.getAllByLitterNameNative(litterName);

    }







//------------Guardar un animal---------------
    @Override
    public AnimalDTO save(AnimalForm form) {
        Animal animal = new Animal();
        // 1. guardar Animal
        animal.setName((form.getName()));
        animal.setWeight(form.getWeight());
        animal.setBirtdate(form.getBirtdate());
        animal.setBirthDate(form.getBirthDate());
        animal = animalRepository.save(animal);

        // 2. validar si la camada existe
        Litter litter = litterRepository.findById(form.getIdLitter())
                .orElseThrow(() -> new IllegalArgumentException("no existe el id camada"));

        // 3. guardar en tabla de relacion id camada e id animal
        AnimalLitter animalLitter = new AnimalLitter();
        animalLitter.setIdLitter(litter.getId());
        animalLitter.setIdAnimal(animal.getId());

        animalLitterRepository.save(animalLitter);

        AnimalDTO responseDTO = new AnimalDTO();

        responseDTO.setId(animal.getId());
        responseDTO.setName(animal.getName());
        responseDTO.setWeight(animal.getWeight());
        responseDTO.setBirtdate(animal.getBirtdate());
        responseDTO.setBirthDate(animal.getBirthDate());

        LitterDTO litterDTO = new LitterDTO();
        litterDTO.setId(litter.getId());
        litterDTO.setName(litter.getName());
        litterDTO.setDescription(litterDTO.getDescription());
        responseDTO.setLitter(litterDTO);

        return responseDTO;
    }

//------------------***------------------


    @Override
    public boolean deleteById(Long id) {
        // 1. que exista el animal
        Animal animal = animalRepository.findById(id).
                orElseThrow(() -> new IllegalArgumentException("no existe el id del animal"));
        List<AnimalLitter> animalLitterList = animalLitterRepository.findAllByIdAnimal(animal.getId());
        animalLitterRepository.deleteAll(animalLitterList);
        animalRepository.deleteById(animal.getId());
        return true;
    }

    //---------Update-----------

    @Override
    public AnimalDTO update(AnimalForm form, Long id) {
        Animal oldAnimal = animalRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("No se encontro el animal"));
        // 1. actualizar animal
        oldAnimal.setName((form.getName()));
        oldAnimal.setWeight(form.getWeight());
        oldAnimal.setBirtdate(form.getBirtdate());
        oldAnimal = animalRepository.save(oldAnimal);

        AnimalDTO responseDTO = new AnimalDTO();

        responseDTO.setId(oldAnimal.getId());
        responseDTO.setName(oldAnimal.getName());
        responseDTO.setWeight(oldAnimal.getWeight());
        responseDTO.setBirtdate(oldAnimal.getBirtdate());

        return responseDTO;
    }


    //---------UpdateMasive-------------

    @Override
    public List<AnimalDTO> updateMasive(MasiveAnimalUpdateForm form) {
        List<AnimalDTO> responseList = new ArrayList<>();
        form.getFormList().forEach(
                updateForm -> responseList.add(update(updateForm, updateForm.getId())));
        return responseList;
    }
}

















