package com.ucc.porkiApp.service;

import com.ucc.porkiApp.domain.dto.AnimalDTO;
import com.ucc.porkiApp.domain.dto.AnimalForm;
import com.ucc.porkiApp.domain.dto.MasiveAnimalUpdateForm;
import com.ucc.porkiApp.domain.entity.Animal;

import java.util.List;

public interface AnimalService {

    List<Animal> getAllAnimalsOrderByName();

    List<Animal> getAllAnimalsByLitterName(String litterName);

    Animal getByName(String name);

    AnimalDTO save(AnimalForm form);

    boolean deleteById(Long id);

    AnimalDTO update(AnimalForm form, Long id);

    List<AnimalDTO> updateMasive(MasiveAnimalUpdateForm form);

































}
