package com.ucc.porkiApp.service;

import com.ucc.porkiApp.domain.entity.Animal;

import java.util.List;

public interface ProductService {
    List<Animal> findAll();
}
