package com.ucc.porkiApp.service;

import com.ucc.porkiApp.domain.dto.*;
import com.ucc.porkiApp.domain.entity.Animal;
import com.ucc.porkiApp.domain.entity.AnimalLitter;
import com.ucc.porkiApp.domain.entity.Equipment;
import com.ucc.porkiApp.domain.entity.Litter;
import com.ucc.porkiApp.domain.repository.AnimalLitterRepository;
import com.ucc.porkiApp.domain.repository.LitterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service // Lo expone como un servicio
@Transactional // punto a punto (commit) si no hace le comit -> rollback --> Es un ctrl Z en bd

public class LitterServiceImpl implements LitterService{

    @Autowired //Patron de inyeccion de dependencias- Aqui se ine los servicios con el dominio
    private LitterRepository litterRepository;

    @Autowired
    private AnimalLitterRepository animalLitterRepository;


    @Override
    public List<Litter> findAll() {
        return litterRepository.findAll();
    }

    @Override
    public Optional<Litter> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public Litter save(Litter category) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Litter getByName(String name) {
        return null;
    }



    //----------Guardar camada--------------
    @Override
    public LitterDTO save(LitterForm form) {
        Litter litter = new Litter();
        // 1. guardar camada
        litter.setName(form.getName());
        litter.setDescription(form.getDescription());
        litter = litterRepository.save(litter);

        LitterDTO respondeDTO = new LitterDTO();

        respondeDTO.setId(litter.getId());
        respondeDTO.setName(litter.getName());
        respondeDTO.setDescription(litter.getDescription());

        return respondeDTO;
    }

    //----------Eliminar camada--------------
    @Override
    public boolean deleteLitterById(Long id) {
        Litter litter = litterRepository.findById(id).
                orElseThrow(() -> new IllegalArgumentException("no existe el id de la camada"));
        litterRepository.deleteById(litter.getId());

        return true;
    }


    //----------Actualizar camada--------------

    @Override
    public LitterDTO update(LitterForm form, Long id) {
        Litter oldLitter = litterRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("No se encontro la camada"));
        // 1. actualizar camada
        oldLitter.setName((form.getName()));
        oldLitter.setDescription(form.getDescription());

        oldLitter = litterRepository.save(oldLitter);

        LitterDTO responseDTO = new LitterDTO();

        responseDTO.setId(oldLitter.getId());
        responseDTO.setName(oldLitter.getName());
        responseDTO.setDescription(oldLitter.getDescription());

        return responseDTO;
    }

    //----------Actualizar masivamente una camada--------------
    @Override
    public List<LitterDTO> updateMasive(MasiveLitterUpdateForm form) {
        List<LitterDTO> responseList = new ArrayList<>();
        form.getFormList().forEach(
                updateForm -> responseList.add(update(updateForm, updateForm.getId())));

        return responseList;
    }
}


























