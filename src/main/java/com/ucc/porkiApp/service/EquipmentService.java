package com.ucc.porkiApp.service;

import com.ucc.porkiApp.domain.dto.*;
import com.ucc.porkiApp.domain.entity.Equipment;

import java.util.List;

public interface EquipmentService {

    List<Equipment> getAllByNameIsLike(String name);

    List<Equipment> getAllEquipmentsOrderByName();

    Equipment getByName(String name);

    EquipmentDTO save(EquipmentForm form);

    boolean deleteById(Long id);

    EquipmentDTO update(EquipmentForm form, Long id);

    List<EquipmentDTO> updateMasive(MasiveEquipmentUpdateForm form);


}


























