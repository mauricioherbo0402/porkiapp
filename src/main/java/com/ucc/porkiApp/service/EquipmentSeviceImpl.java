package com.ucc.porkiApp.service;

import com.ucc.porkiApp.domain.dto.*;
import com.ucc.porkiApp.domain.entity.Animal;
import com.ucc.porkiApp.domain.entity.AnimalLitter;
import com.ucc.porkiApp.domain.entity.Equipment;
import com.ucc.porkiApp.domain.entity.Litter;
import com.ucc.porkiApp.domain.repository.EquipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class EquipmentSeviceImpl implements EquipmentService {

    @Autowired
    private EquipmentRepository equipmentRepository;

    @Override
    public List<Equipment> getAllByNameIsLike(String name) {
        return equipmentRepository.findByNameContains(name);
    }

    @Override
    public List<Equipment> getAllEquipmentsOrderByName() {
        return equipmentRepository.getAllEquipmentsOrderByName();
    }

    @Override
    public Equipment getByName(String name) {
        return this.equipmentRepository.getByName(name).orElse(new Equipment()); }


    //------------Guardar un material---------------
    @Override
    public EquipmentDTO save(EquipmentForm form) {
        Equipment equipment = new Equipment();
        // 1. guardar material
        equipment.setAmount(form.getAmount());
        equipment.setName(form.getName());
        equipment.setDescription(form.getDescription());

        equipment = equipmentRepository.save(equipment);

        EquipmentDTO respondeDTO = new EquipmentDTO();

        respondeDTO.setId(equipment.getId());
        respondeDTO.setAmount(equipment.getAmount());
        respondeDTO.setName(equipment.getName());
        respondeDTO.setDescription(equipment.getDescription());

        return respondeDTO;
    }

    //------------eliminar un material---------------
    @Override
    public boolean deleteById(Long id) {
        Equipment equipment = equipmentRepository.findById(id).
                orElseThrow(() -> new IllegalArgumentException("no existe el id del material"));

        equipmentRepository.deleteById(equipment.getId());
        return true;
    }

    //------------actualizar un material---------------


    @Override
    public EquipmentDTO update(EquipmentForm form, Long id) {
        Equipment oldEquipment = equipmentRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("No se encontro el material"));
        oldEquipment.setName((form.getName()));
        oldEquipment.setDescription(form.getDescription());
        oldEquipment.setAmount(form.getAmount());

        oldEquipment = equipmentRepository.save(oldEquipment);

        EquipmentDTO responseDTO = new EquipmentDTO();

        responseDTO.setId(oldEquipment.getId());
        responseDTO.setName(oldEquipment.getName());
        responseDTO.setDescription(oldEquipment.getDescription());
        responseDTO.setAmount(oldEquipment.getAmount());

        return responseDTO;
    }
    //--------Actualizar un material masivamente
    @Override
    public List<EquipmentDTO> updateMasive(MasiveEquipmentUpdateForm form) {
        List<EquipmentDTO> responseList = new ArrayList<>();
        form.getFormList().forEach(
                updateForm -> responseList.add(update(updateForm, updateForm.getId())));
        return responseList;
    }
}

























