package com.ucc.porkiApp.service;

import com.ucc.porkiApp.domain.dto.*;
import com.ucc.porkiApp.domain.entity.Litter;

import java.util.List;
import java.util.Optional;

public interface LitterService {
    //Capa de logica de negocio-programacion-Servicios(Microservicios)

    List<Litter> findAll();

    Optional<Litter> findById(Long id);

    Litter save (Litter category);

    void deleteById(Long id);

    Litter getByName(String name);

    LitterDTO save(LitterForm form);

    boolean deleteLitterById(Long id);

    LitterDTO update(LitterForm form, Long id);

    List<LitterDTO> updateMasive(MasiveLitterUpdateForm form);
























}
