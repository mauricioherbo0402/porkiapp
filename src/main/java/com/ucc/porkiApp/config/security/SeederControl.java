package com.ucc.porkiApp.config.security;

import com.ucc.porkiApp.domain.entity.User;
import com.ucc.porkiApp.domain.repository.UserRepository;
import com.ucc.porkiApp.enums.RegisterStatusEnum;
import com.ucc.porkiApp.enums.UserRolEnum;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Component
public class SeederControl {
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private UserRepository userRepository;

    public SeederControl(BCryptPasswordEncoder bCryptPasswordEncoder, UserRepository userRepository) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepository = userRepository;
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                new AuthenticatedUser(1L, "a@a.com"),
                "key"
        ));
    }

    @EventListener
    @Transactional
    public void seed(ContextRefreshedEvent contextRefreshedEvent) {
        this.userSeed();
    }

    protected void userSeed() {
        User user = new User();
        user.setEmail("a@b.com");
        user.setFullName("Test test");
        user.setIdentification("5");
        user.setUserRolEnum(UserRolEnum.ADMIN);
        System.out.println("***************************************************");
        String encode = bCryptPasswordEncoder.encode("pruebas");
        System.out.println(encode);
        user.setPassword(encode);
        user.setPhone("456456");
        user.setCreatedAt(new Date());
        user.setCreatedBy(1L);
        user.setStatus(RegisterStatusEnum.ACTIVE);

        User newUserCreated = this.userRepository.save(user);
        System.out.println(newUserCreated);
    }
}
