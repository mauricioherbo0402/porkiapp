package com.ucc.porkiApp.config.security;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ucc.porkiApp.enums.TokenTypeEnum;

import java.util.Date;

public class GeneratedAccessToken {
    private String token;
    private TokenTypeEnum type;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSz")
    private Date expirationDate;

    public GeneratedAccessToken(String token, TokenTypeEnum type, Date expirationDate) {
        this.token = token;
        this.type = type;
        this.expirationDate = expirationDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public TokenTypeEnum getType() {
        return type;
    }

    public void setType(TokenTypeEnum type) {
        this.type = type;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}
