package com.ucc.porkiApp.domain.repository;

import com.ucc.porkiApp.domain.entity.AnimalLitter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnimalLitterRepository extends JpaRepository<AnimalLitter, Long> {

    List<AnimalLitter> findAllByIdAnimal(Long idAnimal);

    List<AnimalLitter> findAllByIdLitter(Long idLitter);

}
