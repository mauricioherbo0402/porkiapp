package com.ucc.porkiApp.domain.repository;

import com.ucc.porkiApp.domain.entity.Litter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LitterRepository extends JpaRepository<Litter, Long> {
    //Capa acceso a datos-Reósitorio de consultas-DAO
    //CRUD A NIVEL DE SPRINGBOOT
    @Override
    List<Litter> findAll();

    Optional<Litter> findById(Long id);

    Litter save (Litter category);

    void deleteById(Long id);

}










