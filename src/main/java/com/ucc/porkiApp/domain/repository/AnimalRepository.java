package com.ucc.porkiApp.domain.repository;

import com.ucc.porkiApp.domain.entity.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {

    List<Animal> findAllByOrderByName();
    //select * from pig pg order by pg.name;
    @Query(value = "select an from Animal an order by an.name")
    List<Animal> getAllOrderByName();


    @Query(value = "select a from Animal a " +
            "inner join AnimalLitter al on (a.id = al.idAnimal)" +
            "inner join Litter l on (l.id = al.idLitter)" +
            "where l.name = :litterName order by a.weight desc")
    List<Animal> getAllByLitterNameNative(@Param("litterName") String litterName);

    @Query(value = "select a from Animal a where a.name = :name")
    Optional<Animal> getByName(@Param("name") String name);




}


























































