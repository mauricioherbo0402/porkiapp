package com.ucc.porkiApp.domain.repository;

import com.ucc.porkiApp.domain.entity.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {

    List<Equipment> findByNameContains(String name);

    @Query(value = "select eq from Equipment eq where eq.name like : name")
    List<Equipment> getAllByNameIsLike(@Param("name") String name);


    List<Equipment> findAllEquipmentsByOrderByName();
    //select * from equipment eq order by eq.name;
    @Query(value = "select eq from Equipment eq order by eq.name")
    List<Equipment> getAllEquipmentsOrderByName();


    @Query(value = "select eq from Equipment eq where eq.name = :name")
    Optional<Equipment> getByName(@Param("name") String name);

}

























