package com.ucc.porkiApp.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;


@Entity
@Table(name = "animal_litter", schema = "shopping",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "animal_litter_un", columnNames = {"id_litter", "id_animal"})
        })
public class AnimalLitter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "id_litter", nullable = false)
    private Long idLitter;

    @JoinColumn(name = "id_litter", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JsonIgnore
    private Litter litter;

    //----------------------------------------------------------------------------
    @Column(name = "id_animal", nullable = false)
    private Long idAnimal;

    @JoinColumn(name = "id_animal", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Animal animal;


    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getIdLitter() { return idLitter; }

    public void setIdLitter(Long idLitter) { this.idLitter = idLitter; }

    public Litter getLitter() { return litter; }

    public void setLitter(Litter litter) { this.litter = litter; }

    public Long getIdAnimal() { return idAnimal; }

    public void setIdAnimal(Long idAnimal) { this.idAnimal = idAnimal; }

    public Animal getAnimal() { return animal; }

    public void setAnimal(Animal animal) { this.animal = animal; }
}
