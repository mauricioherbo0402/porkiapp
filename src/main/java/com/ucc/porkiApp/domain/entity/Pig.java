package com.ucc.porkiApp.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;



@Entity(name = "pig")
public class Pig extends AbstractEntity{

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "weight", nullable = false)
    private BigDecimal weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
