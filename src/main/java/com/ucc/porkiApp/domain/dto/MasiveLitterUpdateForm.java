package com.ucc.porkiApp.domain.dto;

import java.util.List;

public class MasiveLitterUpdateForm {
    private List<LitterForm> formList;

    public List<LitterForm> getFormList() {
        return formList;
    }

    public void setFormList(List<LitterForm> formList) {
        this.formList = formList;
    }
}
