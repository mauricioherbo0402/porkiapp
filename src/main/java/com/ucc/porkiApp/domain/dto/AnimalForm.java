package com.ucc.porkiApp.domain.dto;

import java.util.Date;

/**
 * POJO
 */
public class AnimalForm {
    private Long id;
    private String name;
    private Double weight;
    private String birtdate;
    private Date birthDate;
    private Long idLitter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getBirtdate() {
        return birtdate;
    }

    public void setBirtdate(String birtdate) {
        this.birtdate = birtdate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Long getIdLitter() {
        return idLitter;
    }

    public void setIdLitter(Long idLitter) {
        this.idLitter = idLitter;
    }
}
