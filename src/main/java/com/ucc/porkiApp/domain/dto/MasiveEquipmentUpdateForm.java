package com.ucc.porkiApp.domain.dto;

import java.util.List;

public class MasiveEquipmentUpdateForm {
    private List<EquipmentForm> formList;

    public List<EquipmentForm> getFormList() {
        return formList;
    }

    public void setFormList(List<EquipmentForm> formList) {
        this.formList = formList;
    }
}
