package com.ucc.porkiApp.domain.dto;

import java.util.List;

public class MasiveAnimalUpdateForm {
    private List<AnimalForm> formList;

    public List<AnimalForm> getFormList() {
        return formList;
    }

    public void setFormList(List<AnimalForm> formList) {
        this.formList = formList;
    }
}
