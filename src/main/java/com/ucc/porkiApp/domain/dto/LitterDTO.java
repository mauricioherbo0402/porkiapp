package com.ucc.porkiApp.domain.dto;

public class LitterDTO {
    private Long id;
    private String name;
    private String description;
    private LitterDTO litter;

    public LitterDTO getLitter() {
        return litter;
    }

    public void setLitter(LitterDTO litter) {
        this.litter = litter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
