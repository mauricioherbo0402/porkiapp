package com.ucc.porkiApp.enums;

public enum UserRolEnum {
    ADMIN, CUSTOMER, SELLER
}
