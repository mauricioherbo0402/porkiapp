package com.ucc.porkiApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication

@EntityScan("com.ucc.porkiApp.domain")
public class PorkiAppApplication {
	// Cambios para probar Git
	public static void main(String[] args) {
		SpringApplication.run(PorkiAppApplication.class, args);
	}

}



