package com.ucc.porkiApp.controller;


import com.ucc.porkiApp.domain.dto.*;
import com.ucc.porkiApp.domain.entity.Equipment;
import com.ucc.porkiApp.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EquipmentController {

    @Autowired
    private EquipmentService equipmentService;


    @GetMapping("equipments/{name}")
    public ResponseEntity<List<Equipment>> getAllByNameIsLike(@PathVariable String name) {
        return ResponseEntity.ok(equipmentService.getAllByNameIsLike(name));
    }

    @GetMapping("equipments")
    public ResponseEntity<List<Equipment>> findAll(){
        return ResponseEntity.ok(equipmentService.getAllEquipmentsOrderByName());
    }

    @GetMapping("equipment-search/{name}")
    public ResponseEntity<Equipment> getByName(@PathVariable String name) {
        Equipment equipment = this.equipmentService.getByName(name);

        if (equipment.getId() != null)
            return ResponseEntity.ok(equipment);
        else
            return ResponseEntity.notFound().build();
    }

//----------New--------------
    @PostMapping("equipment")
    public ResponseEntity<EquipmentDTO> save(@RequestBody EquipmentForm form) {
        try {
            EquipmentDTO equipment = this.equipmentService.save(form);
            if (equipment.getId() != null)
                return ResponseEntity.ok(equipment);
            else
                return ResponseEntity.badRequest().build();
        }catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("equipment/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        try {
            boolean isDeleted = this.equipmentService.deleteById(id);

            if (isDeleted)
                return ResponseEntity.ok("Fue borrado con exito el material " + id);
            else
                return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }

    }


    @PutMapping("equipment/{id}")
    public ResponseEntity<EquipmentDTO> updateEquipment(@RequestBody EquipmentForm form, @PathVariable Long id) {
        try {
            EquipmentDTO equipment = this.equipmentService.update(form, id);
            if (equipment.getId() != null)
                return ResponseEntity.ok(equipment);
            else
                return ResponseEntity.badRequest().build();
        }catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("equipment/masive")
    public ResponseEntity<List<EquipmentDTO>> updateMasiveEquipment(@RequestBody MasiveEquipmentUpdateForm form) {
        try {
            List<EquipmentDTO> equipmentDTOList = this.equipmentService.updateMasive(form);
            if (!equipmentDTOList.isEmpty())
                return ResponseEntity.ok(equipmentDTOList);
            else
                return ResponseEntity.badRequest().build();
        }catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }





































}
