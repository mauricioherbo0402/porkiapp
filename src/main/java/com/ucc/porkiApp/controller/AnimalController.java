package com.ucc.porkiApp.controller;

import com.ucc.porkiApp.domain.dto.AnimalDTO;
import com.ucc.porkiApp.domain.dto.AnimalForm;
import com.ucc.porkiApp.domain.dto.MasiveAnimalUpdateForm;
import com.ucc.porkiApp.domain.entity.Animal;
import com.ucc.porkiApp.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
public class AnimalController {

    @Autowired
    private AnimalService animalService;


    @GetMapping("animals")
    public ResponseEntity<List<Animal>> findAll(){
        return ResponseEntity.ok(animalService.getAllAnimalsOrderByName());
    }


    @GetMapping("litter/{litterName}/animals ")
    public ResponseEntity<List<Animal>> getAllAnimalsByLitterName(@PathVariable String litterName) {
        return ResponseEntity.ok(animalService.getAllAnimalsByLitterName(litterName));
    }

    @GetMapping("animal-search/{name}")
    public ResponseEntity<Animal> getByName(@PathVariable String name) {
        Animal animal = this.animalService.getByName(name);

        if (animal.getId() != null)
            return ResponseEntity.ok(animal);
        else
            return ResponseEntity.notFound().build();
    }

//-----------------------------------------------------------

    @PostMapping("animal")
    public ResponseEntity<AnimalDTO> save(@RequestBody AnimalForm form) {
        try {
            AnimalDTO animal = this.animalService.save(form);
            if (animal.getId() != null)
                return ResponseEntity.ok(animal);
            else
                return ResponseEntity.badRequest().build();
        }catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("animal/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        try {
            boolean isDeleted = this.animalService.deleteById(id);

            if (isDeleted)
                return ResponseEntity.ok("Fue borrado con exito el animal " + id);
            else
                return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("animal/{id}")
    public ResponseEntity<AnimalDTO> updateAnimal(@RequestBody AnimalForm form, @PathVariable Long id) {
        try {
            AnimalDTO animal = this.animalService.update(form, id);
            if (animal.getId() != null)
                return ResponseEntity.ok(animal);
            else
                return ResponseEntity.badRequest().build();
        }catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("animal/masive")
    public ResponseEntity<List<AnimalDTO>> updateMasiveAnimal(@RequestBody MasiveAnimalUpdateForm form) {
        try {
            List<AnimalDTO> animalDTOList = this.animalService.updateMasive(form);
            if (!animalDTOList.isEmpty())
                return ResponseEntity.ok(animalDTOList);
            else
                return ResponseEntity.badRequest().build();
        }catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }



















}






















