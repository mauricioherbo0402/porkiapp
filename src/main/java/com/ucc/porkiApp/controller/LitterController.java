package com.ucc.porkiApp.controller;

import com.ucc.porkiApp.domain.dto.*;
import com.ucc.porkiApp.domain.entity.Litter;
import com.ucc.porkiApp.service.LitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Este es el API, LO expuesto -> Interfaz de comunicacion.
@RestController//vamos a exponer unos endpoints


public class LitterController {

    // CAPA DE PRESENTACION - CONTROLADORES TIPO REST - ENDPOINTS
    @Autowired
    private LitterService litterService;

    @GetMapping("litters")
    public ResponseEntity<List<Litter>> findAll(){
        return ResponseEntity.ok(litterService.findAll());
    }




    //----------New-----------

    @PostMapping("litter")
    public ResponseEntity<LitterDTO> save(@RequestBody LitterForm form) {
        try {
            LitterDTO litter = this.litterService.save(form);
            if (litter.getId() != null)
                return ResponseEntity.ok(litter);
            else
                return ResponseEntity.badRequest().build();
        }catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("litter/{id}")
    public ResponseEntity<String> deleteLitterById(@PathVariable Long id) {
        try {
            boolean isDeleted = this.litterService.deleteLitterById(id);

            if (isDeleted)
                return ResponseEntity.ok("Fue borrado con exito la camada " + id);
            else
                return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("litter/{id}")
    public ResponseEntity<LitterDTO> updateLitter(@RequestBody LitterForm form, @PathVariable Long id) {
        try {
            LitterDTO litter = this.litterService.update(form, id);
            if (litter.getId() != null)
                return ResponseEntity.ok(litter);
            else
                return ResponseEntity.badRequest().build();
        }catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("litter/masive")
    public ResponseEntity<List<LitterDTO>> updateMasiveLitter(@RequestBody MasiveLitterUpdateForm form) {
        try {
            List<LitterDTO> litterDTOList = this.litterService.updateMasive(form);
            if (!litterDTOList.isEmpty())
                return ResponseEntity.ok(litterDTOList);
            else
                return ResponseEntity.badRequest().build();
        }catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }





























}
